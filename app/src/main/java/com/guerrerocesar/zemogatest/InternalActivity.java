package com.guerrerocesar.zemogatest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.guerrerocesar.zemogatest.adapter.PostListAdapter;
import com.guerrerocesar.zemogatest.controller.AppController;
import com.guerrerocesar.zemogatest.model.Post;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class InternalActivity extends AppCompatActivity {

    // Log tag
    private static final String TAG = MainActivity.class.getSimpleName();

    private android.support.v7.app.ActionBar actionBar;

    private TextView descriptionText;
    private TextView userName;
    private TextView userEmail;
    private TextView userPhone;
    private TextView userWebsite;
    private ListView commentsList;
    private Post currentPost;

    String favoriteMarked = "Marked as Favorite";
    String favoriteUnmarked = "Favorite Removed";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internal);

        actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        descriptionText = findViewById(R.id.description_text);
        userName = findViewById(R.id.user_name);
        userEmail = findViewById(R.id.user_email);
        userPhone = findViewById(R.id.user_phone);
        userWebsite = findViewById(R.id.user_website);
        commentsList = findViewById(R.id.comments_listView);

        getStoredInfo();
        getUserInfo();
        getComments();
    }

    private void getStoredInfo() {
        PostListAdapter adapter = AppController.getInstance().getAdapter();
        Intent intent = getIntent();

        int currentPostId = intent.getIntExtra("POST_POSITION", -1);

        if (currentPostId >= 0)  {
            this.currentPost = (Post) adapter.getItem(currentPostId);
            descriptionText.setText(currentPost.getBody());
        }
    }

    private void getUserInfo() {
        String userURL = AppController.URL + "/users/" + this.currentPost.getUserId();

        final JsonObjectRequest userRequest = new JsonObjectRequest(Request.Method.GET, userURL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.v(TAG, "Response User: " + response.toString());

                try {

                    userName.setText(response.getString("name"));
                    userEmail.setText(response.getString("email"));
                    userPhone.setText(response.getString("phone"));
                    userWebsite.setText(response.getString("website"));

                } catch (JSONException error) {
                    error.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });

        AppController.getInstance().addToRequestQueue(userRequest);
    }

    private void getComments() {
        String commentsURL = AppController.URL + "/comments?postId=" + this.currentPost.getUserId();

        final JsonArrayRequest commentsRequest = new JsonArrayRequest(Request.Method.GET, commentsURL, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.v(TAG, "Response Comments: " + response.toString());

                try {
                    String[] comments = new String[response.length()];

                    for (int i = 0; i < response.length(); i++) {
                        comments[i] = response.getJSONObject(i).getString("body");
                    }

                    commentsList.setAdapter(new ArrayAdapter<>(getApplicationContext(), R.layout.comment_item, comments));

                } catch (JSONException error) {
                    error.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        AppController.getInstance().addToRequestQueue(commentsRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        // Mark post as read
        this.currentPost.setAlreadyRead(true);
        AppController.getInstance().getAdapter().notifyDataSetChanged();

        finish();
        overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.internal_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite:
                this.currentPost.setFavorite(!this.currentPost.getFavorite());

                String notifyText = this.currentPost.getFavorite() ? favoriteMarked : favoriteUnmarked;
                Toast.makeText(this, notifyText, Toast.LENGTH_SHORT).show();

                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
