package com.guerrerocesar.zemogatest.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.guerrerocesar.zemogatest.InternalActivity;
import com.guerrerocesar.zemogatest.MainActivity;
import com.guerrerocesar.zemogatest.R;
import com.guerrerocesar.zemogatest.controller.AppController;
import com.guerrerocesar.zemogatest.model.Post;

import java.util.ArrayList;
import java.util.List;

public class PostListAdapter extends RecyclerView.Adapter<PostListAdapter.ViewHolder> {

    private Activity activity;
    private List<Post> postList;

    final static int newPosts = 20;

    public PostListAdapter(Activity activity, List<Post> postList) {
        this.activity = activity;
        this.postList = postList;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView circleIcon, starIcon;
        TextView postTitle;
        RelativeLayout viewBackground;
        public RelativeLayout viewForeground;

        ViewHolder(View itemView) {
            super(itemView);

            circleIcon = itemView.findViewById(R.id.blue_circle);
            starIcon = itemView.findViewById(R.id.star_fav);
            postTitle = itemView.findViewById(R.id.post_title);
            viewForeground = itemView.findViewById(R.id.view_foreground);
            viewBackground = itemView.findViewById(R.id.view_background);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Post currentPost = postList.get(position);

        // Show Blue circle
        if (position < newPosts && !currentPost.getAlreadyRead()) {
            holder.circleIcon.setVisibility(View.VISIBLE);
            holder.postTitle.setPadding((int) convertDpToPixel(20), holder.postTitle.getPaddingTop(), holder.postTitle.getPaddingRight(), holder.postTitle.getPaddingBottom());
        } else {
            holder.circleIcon.setVisibility(View.GONE);
            holder.postTitle.setPadding((int) convertDpToPixel(8), holder.postTitle.getPaddingTop(), holder.postTitle.getPaddingRight(), holder.postTitle.getPaddingBottom());
        }

        // Show favorite Star
        if (currentPost.getFavorite()) {
            holder.starIcon.setVisibility(View.VISIBLE);
        } else {
            holder.starIcon.setVisibility(View.GONE);
        }

        holder.postTitle.setText(currentPost.getTitle());

        holder.viewForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, InternalActivity.class);

                intent.putExtra("POST_POSITION", holder.getAdapterPosition());

                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            }
        });
    }

    public static float convertDpToPixel(int dp){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public Object getItem(int position) {
        return postList.get(position);
    }

    public void removeItem(int position) {
        postList.remove(position);
        // notify the item removed
        notifyItemRemoved(position);
    }

    public void removeAll() {
        postList.clear();
        notifyDataSetChanged();
    }

    public List<Post> getFavorites() {
        List<Post> favPosts = new ArrayList<>();

        for(Post post: postList) {
            if (post.getFavorite()) {
                favPosts.add(post);
            }
        }

        return favPosts;
    }
}
