package com.guerrerocesar.zemogatest.model;

public class Post {
    private String userId;
    private String id;
    private String title;
    private String body;
    private Boolean favorite;
    private Boolean alreadyRead;

    public Post(String userId, String id, String title, String body) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
        this.favorite = false;
        this.alreadyRead = false;
    }

    public String getUserId() {
        return userId;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public Boolean getAlreadyRead() {
        return alreadyRead;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public void setAlreadyRead(Boolean alreadyRead) {
        this.alreadyRead = alreadyRead;
    }
}
