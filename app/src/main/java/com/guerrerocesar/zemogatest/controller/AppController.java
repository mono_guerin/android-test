package com.guerrerocesar.zemogatest.controller;

import android.app.Application;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.Adapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.guerrerocesar.zemogatest.MainActivity;
import com.guerrerocesar.zemogatest.adapter.PostListAdapter;
import com.guerrerocesar.zemogatest.model.Post;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();
    public static final String URL = "https://jsonplaceholder.typicode.com";
    private static AppController mInstance;

    private RequestQueue mRequestQueue;
    private PostListAdapter adapter;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
    }

    public static synchronized AppController getInstance() {

        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests() {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }

    public void setAdapter(PostListAdapter adapter) {
        this.adapter = adapter;
    }

    public PostListAdapter getAdapter() {
        return this.adapter;
    }

}
