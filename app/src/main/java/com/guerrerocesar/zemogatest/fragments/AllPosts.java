package com.guerrerocesar.zemogatest.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.guerrerocesar.zemogatest.MainActivity;
import com.guerrerocesar.zemogatest.R;
import com.guerrerocesar.zemogatest.adapter.PostListAdapter;
import com.guerrerocesar.zemogatest.controller.AppController;
import com.guerrerocesar.zemogatest.model.Post;
import com.guerrerocesar.zemogatest.utils.RecyclerItemTouchHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AllPosts extends Fragment implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    private PostListAdapter mAdapter;
    private RecyclerView recyclerView;
    private FloatingActionButton deleteBtn;
    private ProgressDialog pDialog;
    private String URL = AppController.URL + "/posts";
    private View mView;

    String allRemovedMsg = "All posts were deleted";
    String loading = "Loading...";

    public AllPosts() {
        // Auto generated
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mView = view;

        deleteBtn = mView.findViewById(R.id.delete_btn);
        recyclerView = mView.findViewById(R.id.listViewPosts);

        initRecyclerView();
        fetchPosts();
        initDeleteBtn();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.posts_list, container, false);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof PostListAdapter.ViewHolder) {
            mAdapter.removeItem(viewHolder.getAdapterPosition());
        }
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        // adding item touch helper
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
    }

    private void initDeleteBtn() {
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.removeAll();
                Toast.makeText(getActivity(), allRemovedMsg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchPosts() {
        Log.v(TAG, "fetching....");

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage(loading);
        pDialog.show();

        final JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.GET, URL, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.v(TAG, "RES: " + response.toString());
                hidePDialog();

                List<Post> postList = new ArrayList<>();

                try {
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject currentPost = response.getJSONObject(i);
                        String userId = currentPost.getString("userId");
                        String id = currentPost.getString("id");
                        String title = currentPost.getString("title");
                        String body = currentPost.getString("body");

                        Post post = new Post(userId, id, title, body);

                        postList.add(post);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.v(TAG, postList.get(2).getTitle());

                mAdapter = new PostListAdapter(getActivity(), postList);
                recyclerView.setAdapter(mAdapter);


                // Storing adapter
                AppController.getInstance().setAdapter(mAdapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
            }
        });

        AppController.getInstance().addToRequestQueue(jsonRequest);
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                fetchPosts();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
