package com.guerrerocesar.zemogatest.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.guerrerocesar.zemogatest.R;
import com.guerrerocesar.zemogatest.adapter.PostListAdapter;
import com.guerrerocesar.zemogatest.controller.AppController;
import com.guerrerocesar.zemogatest.model.Post;

import java.util.List;

public class FavoritePosts extends Fragment {

    private PostListAdapter mAdapter;
    private View mView;
    private RecyclerView recyclerView;
    private FloatingActionButton deleteBtn;

    List<Post> postList;

    public FavoritePosts() {
        // Auto generated
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mView = view;

        deleteBtn = mView.findViewById(R.id.delete_btn);
        recyclerView = mView.findViewById(R.id.listViewPosts);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Hide delete Btn
        deleteBtn.setVisibility(View.GONE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.posts_list, container, false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
            loadFavorites();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item=menu.findItem(R.id.action_refresh);
        item.setVisible(false);

        super.onPrepareOptionsMenu(menu);
    }

    public void loadFavorites() {
        PostListAdapter tempAdapter = AppController.getInstance().getAdapter();

        // Show just the Favorite Posts
        postList = tempAdapter.getFavorites();
        mAdapter = new PostListAdapter(getActivity(), postList);
        recyclerView.setAdapter(mAdapter);
    }
}
