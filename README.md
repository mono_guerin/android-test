Z-Test Android Technical Test
==================

Basic test application that lists all messages and their details from [JSONPlaceholder](https://jsonplaceholder.typicode.com/)

![Main Screen](/screenshots/main.png)
![Favorites Screen](/screenshots/favorites.png)
![Details Screen](/screenshots/details.png)

# Running the App

## Requirements

- `Android Studio 3.1.2`
- `Gradle 3.1.2`
- `Android SDK 27`
- `JDK 8`

Running the app should be very straightforward, just open this folder with Android Studio and sync gradle for any dependency needed to be downloaded.
Then you can just hit the Play button for an instant run.

> *Note:* An apk will also be supplied with the code so you can just install it in a test android device.

# External Libraries Used

* `Volley` - Volley is an HTTP library that makes networking for Android apps easier and most importantly, faster.
* `Android RecyclerView Library` - This class provides support for the RecyclerView widget, a view for efficiently displaying large data sets by providing a limited window of data items.
* `Android Design Library` - The Design Support library adds support for various material design components and patterns for app developers to build upon, such as navigation drawers, floating action buttons (FAB), snackbars, and tabs.



